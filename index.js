// console.log("Send stars!");

let trainer ={
	name: "Ash Ketchum", 
	age: 10,
	pokemon: ["Pikachu", "Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}

}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name,level){
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2*level;
	this.pokemonAttack = level;
	this.tackle = function(target){
		// hp 0 not able to battle
		if ( target.pokemonHealth <= 0 || this.pokemonHealth <= 0){
			console.log("Pokemon is unable to battle.");
		}
		else {
			// initiate tackle
			console.log(this.pokemonName + " tackled " + target.pokemonName);

			target.pokemonHealth = target.pokemonHealth - this.pokemonAttack;
			if (target.pokemonHealth <= 0){
				target.pokemonHealth = 0;

			console.log(target.pokemonName + " health is now reduced to " + target.pokemonHealth);
			target.faint();
			}
			else{
			console.log(target.pokemonName + " health is now reduced to " + target.pokemonHealth);
			}
		}

	}
	this.faint = function(){
		console.log(this.pokemonName + " fainted.");
	}

}


let Pikachu = new Pokemon("Pikachu",12);
let Geodude = new Pokemon("Geodude",8);
let Mewtwo = new Pokemon("Mewtwo",100);

console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);


Geodude.tackle(Pikachu);
console.log(Pikachu);

Mewtwo.tackle(Geodude);
console.log(Geodude);

Pikachu.tackle(Geodude);

Pikachu.tackle(Mewtwo);


